#include "stdafx.h"
/*
BATTLESHIP GAME IN C BY JULIAN MIES, SINA SCHELLER & IVAN KALININ
*/
char Hauptmenue() {
	char w;
	printf("wollen Sie ein neues Spiel starten? (J/N)\t"); scanf_s("%c", &w);
	if (w == 'J') {
		printf("Das Spiel wird gestartet!\n");
	}
	else if (w == 'N') {
		printf("Das Programm wird beendet");
		Sleep(1000);
	}
	else {
		printf("Programmierfehler oder falsche Eingabe!");
		printf("Das Programm wird in Kuerze beendet");
		Sleep(2000);
		exit(0);
	}
	return w;
}
bool SchiffeSetzen(int spieler,int Schiff_2,int Schiff_3,int Schiff_4,int Schiff_5,int xK,int yK,int *feld){
	unsigned Zufall = time(NULL);
	srand(Zufall);
	int AnzahlDerSchiffe = 7;
	bool vormerken;
	int x, y, r, versuche, art;
	int  dx, dy, i, j;
	for (i = 0; i<AnzahlDerSchiffe; i++){
		if (i<Schiff_2) art = 2;
		else{
			if (i<Schiff_2+Schiff_3) art = 3;
			else art = 4;
		}
		versuche = 0;
		do{ //Schiffe setzen nach dem Zufallsprinzip 
			vormerken = false;
			x = rand() % xK;
			y = rand() % yK;
			r = rand() % 2 + 1;
			if (r == 1) { 
				dx = 0; dy = 1;
			}
			else { 
				dx = 1; dy = 0; 
			}
			for (j = 0; j<art; j++)
				if ((x + j*dx<0) || (x + j*dx >= xK) || (y + j*dy<0) || (y + j*dy >= yK)) vormerken = true;
			if (!vormerken)
				for (j = 0; j<art; j++)
					if (feld[xK*(y + j*dy) + x + j*dx + xK*yK*(spieler - 1)] != 0) vormerken = true;
			versuche++;
		} 
		while ((vormerken) && (versuche<100)); //100 versuche um Schiffe kolissionsfrei zu platzieren
		if (vormerken) return false;
		for (j = 0; j<art; j++)
			feld[xK*(y + j*dy) + x + j*dx + xK*yK*(spieler - 1)] = 1;
	}
	return true;
}
void Abschuss(int spieler, int xK, int yK, int*feld){
	int x = 0, y = 0;
	printf("\nSpieler %d ist dran\n", spieler);
	if (spieler == 1) spieler = 2;
	else spieler = 1;
	printf("\nbitte geben Sie die Schusskoordinaten ein:\n");
	do{
		printf("x Koordinate:\t "); scanf_s("%d", &x);
		printf("y Koordinate:\t "); scanf_s("%d", &y);
	} while ((x<0) || (x >= xK) || (y<0) || (y >= yK));
	if (feld[xK*y + x + xK*yK*(spieler - 1)] == 1)
	{
		printf("\nTreffer!\n");
		feld[xK*y + x + xK*yK*(spieler - 1)] = 2;
	}
	else
	{
		printf("\nDaneben!\n");
		feld[xK*y + x + xK*yK*(spieler - 1)] = 3;
	}
}
bool gewinner(int spieler, int xK, int yK, int*feld){
	for (int i = 0; i<xK*yK; i++)
		if (feld[i + xK*yK*(spieler - 1)] == 1) return false; //sobald ein spieler alle schiffe abgeschossen hat
	return true;
}
		int main() {
			int xK = 10, yK = 10, i, spieler;
			int*feld;
			int Schiff_2=2, Schiff_3=1, Schiff_4=1, Schiff_5=1;
			printf("<-- Schiffe versenken -->\n");
			printf("_... _______$$_§§§.\n");
			printf("___________$$$_§§§§§\n");
			printf("___________$_$_§§§§§§ ………..... ^v^\n");
			printf("__________$$_$__§§§§§§\n");
			printf("_________$$$_$__§§§§§§§\n");
			printf("________$$$$_$__§§§§§§§§\n");
			printf("_______$$$$$_$__§§§§§§§§\n");
			printf("_____$$$$$$$_$__§§§§§§§§§\n");
			printf("____$$$$$$$$_$_§§§§§§§§§§§\n");
			printf("_$_$$$$$$$$$_$_§§§§§§§§§§§\n");
			printf("_$$_$________$$$_____$$$$$___$$\n");
			printf("__$$$$$$$$$$$$$$$$$$$$$$$_$$$$.\n");
			printf("___$$$$$$$$$$$$$$$$$$$$$$$$$$\n");

			do
			{
				if (Hauptmenue() == 'N') break;

				feld = (int *)malloc(xK*yK * 2 * sizeof(int)); //speicherzuiweisung für das feld
				for (i = 0; i<(xK*yK * 2); i++)
					feld[i] = 0;

				SchiffeSetzen(1, Schiff_2, Schiff_3, Schiff_4, Schiff_5, xK, yK, feld); //für spieler 1
				SchiffeSetzen(2, Schiff_2, Schiff_3, Schiff_4, Schiff_5, xK, yK, feld); //für spieler 2
				printf("Schiffe werden nach dem Zufallsprinzip platziert\n fuer Abschuesse geben Sie x y Koordinaten ein von 0-9");
				spieler = 1;
				do
				{
					Abschuss(spieler, xK, yK, feld);
					spieler++;
					if (spieler == 3) spieler = 1;
				} while (!gewinner(spieler, xK, yK, feld)); //sobald gewinner true ergibt, beendet sich das spiel
				spieler++;
				if (spieler == 3) spieler = 1;
				printf("\nSpieler %d hat gewonnen ", spieler);
				free(feld);
			} while (true);
			return 0;
		}
