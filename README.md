Projekt: Schiffe versenken.

In unserem Projekt setzen wir eine konsolenbasierte Version des Spiele Klassikers Schiffe versenken um. 

Das Programm beinhaltet mehrere Funktionen die für den Spielablauf notwendig sind, als Extra Feature wird ein Menü vorhanden sein.

Beim Start des Programms befindet sich der Spieler in einem Menü und wählt anschließend aus ob er ein neues Spiel starten

oder das Programm verlassen möchte.

Danach erzeugt das Programm die Felder und platziert per Zufall die Schiffe der beiden Spieler.

Abgeschossene Felder werden vorgemerkt und können kein zweites Mal abgeschossen werden, dies wird dem Spieler mitgeteilt.

Am Ende wird der Gewinner angezeigt, man kann das Spiel anschließend neustarten.

Die vermutlich größte technische Schwierigkeit wird die grafische Darstellung in der Konsole sein.

